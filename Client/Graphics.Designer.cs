﻿namespace Client
{
    partial class Graphics
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_main = new System.Windows.Forms.RichTextBox();
            this.txt_name = new System.Windows.Forms.TextBox();
            this.txt_msg = new System.Windows.Forms.TextBox();
            this.btn_send = new System.Windows.Forms.Button();
            this.pic_title = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pic_title)).BeginInit();
            this.SuspendLayout();
            // 
            // txt_main
            // 
            this.txt_main.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.txt_main.Font = new System.Drawing.Font("Corbel", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_main.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txt_main.Location = new System.Drawing.Point(154, 90);
            this.txt_main.Margin = new System.Windows.Forms.Padding(2);
            this.txt_main.Name = "txt_main";
            this.txt_main.ReadOnly = true;
            this.txt_main.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.txt_main.Size = new System.Drawing.Size(560, 292);
            this.txt_main.TabIndex = 0;
            this.txt_main.Text = "";
            // 
            // txt_name
            // 
            this.txt_name.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.txt_name.Font = new System.Drawing.Font("Corbel", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_name.ForeColor = System.Drawing.Color.Firebrick;
            this.txt_name.Location = new System.Drawing.Point(154, 425);
            this.txt_name.Margin = new System.Windows.Forms.Padding(2);
            this.txt_name.Name = "txt_name";
            this.txt_name.ReadOnly = true;
            this.txt_name.Size = new System.Drawing.Size(103, 27);
            this.txt_name.TabIndex = 1;
            this.txt_name.Text = "Name";
            this.txt_name.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt_msg
            // 
            this.txt_msg.Font = new System.Drawing.Font("Corbel", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_msg.Location = new System.Drawing.Point(279, 425);
            this.txt_msg.Margin = new System.Windows.Forms.Padding(2);
            this.txt_msg.Name = "txt_msg";
            this.txt_msg.Size = new System.Drawing.Size(329, 27);
            this.txt_msg.TabIndex = 2;
            this.txt_msg.Text = "Message";
            this.txt_msg.Enter += new System.EventHandler(this.txt_msg_TextFocus);
            this.txt_msg.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_msg_KeyDown);
            // 
            // btn_send
            // 
            this.btn_send.BackgroundImage = global::Client.Properties.Resources.button;
            this.btn_send.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_send.Font = new System.Drawing.Font("MV Boli", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_send.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_send.Location = new System.Drawing.Point(630, 416);
            this.btn_send.Margin = new System.Windows.Forms.Padding(2);
            this.btn_send.Name = "btn_send";
            this.btn_send.Size = new System.Drawing.Size(84, 40);
            this.btn_send.TabIndex = 3;
            this.btn_send.Text = "Send";
            this.btn_send.UseVisualStyleBackColor = true;
            this.btn_send.Click += new System.EventHandler(this.sendButton_Click);
            // 
            // pic_title
            // 
            this.pic_title.BackColor = System.Drawing.Color.Transparent;
            this.pic_title.BackgroundImage = global::Client.Properties.Resources.logo;
            this.pic_title.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pic_title.Location = new System.Drawing.Point(32, 12);
            this.pic_title.Name = "pic_title";
            this.pic_title.Size = new System.Drawing.Size(188, 61);
            this.pic_title.TabIndex = 4;
            this.pic_title.TabStop = false;
            // 
            // Graphics
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Client.Properties.Resources.background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(748, 488);
            this.Controls.Add(this.pic_title);
            this.Controls.Add(this.btn_send);
            this.Controls.Add(this.txt_msg);
            this.Controls.Add(this.txt_name);
            this.Controls.Add(this.txt_main);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Graphics";
            this.Text = "Graphics";
            this.Load += new System.EventHandler(this.Graphics_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pic_title)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox txt_main;
        private System.Windows.Forms.TextBox txt_name;
        private System.Windows.Forms.TextBox txt_msg;
        private System.Windows.Forms.Button btn_send;
        private System.Windows.Forms.PictureBox pic_title;
    }
}