﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class Login : Form
    {
        public static string username = "";
        public Login()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, EventArgs e)
        {
            if (txt_Username.Text.Length != 0)
            {
                username = txt_Username.Text;
                this.Hide();
                var chat = new Graphics();
                chat.Closed += (s, args) => this.Close();
                chat.Show();


            }
            else
                MessageBox.Show("Introduceti numele");
        }

        private void txt_Username_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                button_Click(this, new EventArgs());
            }
        }

        private void txt_Username_TextFocus(object sender, EventArgs e)
        {
            txt_Username.Text = "";
        }
    }
}
