﻿using Generated;
using Grpc.Core;
using System;
using System.Media;
using System.Threading;
using System.Windows.Forms;

namespace Client
{
    public partial class Graphics : Form
    {
        private const string Host = "localhost";
        private const int Port = 8080;
        private int clientId;

        private AsyncDuplexStreamingCall<ChatMessage, ChatMessageFromServer> call;
        private ChatService.ChatServiceClient client;
        public Graphics()
        {
            InitializeComponent();
            clientId = getCurrentClientFromFile();
            InitializeGrpc();
        }

        private void InitializeGrpc()
        {
            var channel = new Channel(Host + ":" + Port, ChannelCredentials.Insecure);
            client = new ChatService.ChatServiceClient(channel);
        }

        public int getCurrentClientFromFile()
        {
            System.IO.StreamReader fileReader = new System.IO.StreamReader("..\\..\\..\\ClientCounter/ClientCounter.txt");
            int formId = int.Parse(fileReader.ReadLine());
            fileReader.Close();
            formId += 1;
            System.IO.File.WriteAllText("..\\..\\..\\ClientCounter/ClientCounter.txt", string.Empty);
            System.IO.StreamWriter fileWriter = new System.IO.StreamWriter("..\\..\\..\\ClientCounter/ClientCounter.txt");
            fileWriter.WriteLine(formId);
            fileWriter.Close();
            return formId;
        }
        private async void Graphics_Load(object sender, EventArgs e)
        {
            txt_name.Text = Login.username;
            this.Text = txt_name.Text;
            try
            {
                using (call = client.Chat())
                {
                    while (await call.ResponseStream.MoveNext(CancellationToken.None))
                    {
                        var serverMessage = call.ResponseStream.Current;
                        var otherClientMessage = serverMessage.Message;
                        var displayMessage = string.Format(@"{0} : {1}{2}", otherClientMessage.UserName, otherClientMessage.Message, @"\par ");
                        string rtf = @"{\rtf1\pc " + TextFormatter.FormatBold(TextFormatter.FormatItalic(TextFormatter.FormatStrikethrough(TextFormatter.FormatUnderline(displayMessage))));
                        txt_main.SelectedRtf = rtf;
                        txt_main.SelectionStart = txt_main.Text.Length;
                        txt_main.ScrollToCaret();
                        txt_main.Refresh();
                    }
                }
            }
            catch (RpcException)
            {
                call = null;
                throw;
            }
        }

        private async void sendButton_Click(object sender, EventArgs e)
        {
            var message = new ChatMessage
            {
                UserName = txt_name.Text,
                Message = txt_msg.Text
            };

            if (call != null)
            {
                await call.RequestStream.WriteAsync(message);
                SoundPlayer player = new SoundPlayer("..\\..\\DesignResources/message.wav");
                player.Play();
            }

            txt_msg.Text = "";
        }

        private void txt_msg_TextFocus(object sender, EventArgs e)
        {
            txt_msg.Text = "";
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            client.Disconnect(new User { Id=clientId, UserName = txt_name.Text });
        }

        private void txt_msg_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.SuppressKeyPress = true;
                sendButton_Click(this, new EventArgs());
            }
        }
    }
}
