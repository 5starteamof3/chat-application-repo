﻿namespace Client
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Join = new System.Windows.Forms.Button();
            this.txt_Username = new System.Windows.Forms.TextBox();
            this.pic_welcome = new System.Windows.Forms.PictureBox();
            this.pic_logo = new System.Windows.Forms.PictureBox();
            this.pic_design = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pic_welcome)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_logo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_design)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_Join
            // 
            this.btn_Join.BackgroundImage = global::Client.Properties.Resources.buttonJoin;
            this.btn_Join.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_Join.Font = new System.Drawing.Font("Corbel", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Join.Location = new System.Drawing.Point(321, 322);
            this.btn_Join.Name = "btn_Join";
            this.btn_Join.Size = new System.Drawing.Size(75, 29);
            this.btn_Join.TabIndex = 0;
            this.btn_Join.Text = "Join";
            this.btn_Join.UseVisualStyleBackColor = true;
            this.btn_Join.Click += new System.EventHandler(this.button_Click);
            // 
            // txt_Username
            // 
            this.txt_Username.Font = new System.Drawing.Font("Corbel", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Username.Location = new System.Drawing.Point(272, 135);
            this.txt_Username.Name = "txt_Username";
            this.txt_Username.Size = new System.Drawing.Size(198, 31);
            this.txt_Username.TabIndex = 1;
            this.txt_Username.Text = "Name";
            this.txt_Username.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txt_Username.Enter += new System.EventHandler(this.txt_Username_TextFocus);
            this.txt_Username.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_Username_KeyDown);
            // 
            // pic_welcome
            // 
            this.pic_welcome.BackgroundImage = global::Client.Properties.Resources.welcome;
            this.pic_welcome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pic_welcome.Location = new System.Drawing.Point(-8, 1);
            this.pic_welcome.Name = "pic_welcome";
            this.pic_welcome.Size = new System.Drawing.Size(196, 66);
            this.pic_welcome.TabIndex = 2;
            this.pic_welcome.TabStop = false;
            // 
            // pic_logo
            // 
            this.pic_logo.BackColor = System.Drawing.Color.Transparent;
            this.pic_logo.BackgroundImage = global::Client.Properties.Resources.logo;
            this.pic_logo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pic_logo.Location = new System.Drawing.Point(3, 383);
            this.pic_logo.Name = "pic_logo";
            this.pic_logo.Size = new System.Drawing.Size(132, 44);
            this.pic_logo.TabIndex = 3;
            this.pic_logo.TabStop = false;
            // 
            // pic_design
            // 
            this.pic_design.BackColor = System.Drawing.Color.Transparent;
            this.pic_design.BackgroundImage = global::Client.Properties.Resources.manute;
            this.pic_design.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pic_design.Location = new System.Drawing.Point(-8, 185);
            this.pic_design.Name = "pic_design";
            this.pic_design.Size = new System.Drawing.Size(692, 230);
            this.pic_design.TabIndex = 4;
            this.pic_design.TabStop = false;
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Client.Properties.Resources.loginBackground;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(678, 427);
            this.Controls.Add(this.txt_Username);
            this.Controls.Add(this.btn_Join);
            this.Controls.Add(this.pic_logo);
            this.Controls.Add(this.pic_design);
            this.Controls.Add(this.pic_welcome);
            this.Name = "Login";
            this.Text = "Login";
            ((System.ComponentModel.ISupportInitialize)(this.pic_welcome)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_logo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_design)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Join;
        private System.Windows.Forms.TextBox txt_Username;
        private System.Windows.Forms.PictureBox pic_welcome;
        private System.Windows.Forms.PictureBox pic_logo;
        private System.Windows.Forms.PictureBox pic_design;
    }
}