﻿using System.Text.RegularExpressions;


namespace Client
{
    class TextFormatter
    {
        public static string FormatBold(string text)
        {
            string txt = text;
            Regex r = new Regex(@"( \*.*?\* )");
            foreach (Match m in r.Matches(txt))
            {
                string vechi = m.ToString();
                string nou = vechi.Remove(vechi.Length - 2, 1);
                nou = @"\b " + nou.Remove(1, 1) + @"\b0";
                txt = txt.Replace(vechi, nou);
            }
            return txt;
        }

        public static string FormatItalic(string text)
        {
            string txt = text;
            Regex r = new Regex(@"( _.*?_ )");
            foreach (Match m in r.Matches(txt))
            {
                string vechi = m.ToString();
                string nou = vechi.Remove(vechi.Length - 2, 1);
                nou = @"\i " + nou.Remove(1, 1) + @"\i0";
                txt = txt.Replace(vechi, nou);
            }
            return txt;
        }

        public static string FormatStrikethrough(string text)
        {
            string txt = text;
            Regex r = new Regex(@"( ~.*?~ )");
            foreach (Match m in r.Matches(txt))
            {
                string vechi = m.ToString();
                string nou = vechi.Remove(vechi.Length - 2, 1);
                nou = @"\strike " + nou.Remove(1, 1) + @"\strike0";
                txt = txt.Replace(vechi, nou);
            }
            return txt;
        }

        public static string FormatUnderline(string text)
        {
            string txt = text;
            Regex r = new Regex(@"( `.*?` )");
            foreach (Match m in r.Matches(txt))
            {
                string vechi = m.ToString();
                string nou = vechi.Remove(vechi.Length - 2, 1);
                nou = @"\ul " + nou.Remove(1, 1) + @"\ul0";
                txt = txt.Replace(vechi, nou);
            }
            return txt;
        }
    }
}
