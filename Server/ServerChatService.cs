﻿using Generated;
using Grpc.Core;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Server
{
    public class ServerChatService : ChatService.ChatServiceBase
    {
        private static Dictionary<int,IServerStreamWriter<ChatMessageFromServer>> responses = new Dictionary<int, IServerStreamWriter<ChatMessageFromServer>>();
        int conectedUsers = 0;
        public override async Task Chat(IAsyncStreamReader<ChatMessage> requestStream, IServerStreamWriter<ChatMessageFromServer> responseStream, ServerCallContext context)
        {
            Console.WriteLine("An user has joined the chat!");
            responses.Add(++conectedUsers,responseStream);
            while (await requestStream.MoveNext(CancellationToken.None))
            {
                var messageReceieved = requestStream.Current;
                var message = new ChatMessageFromServer
                {
                    Message = messageReceieved
                };
                foreach (var response in responses)
                {
                    await response.Value.WriteAsync(message);
                }
            }
        }

        public override Task<Empty> Disconnect(User request, ServerCallContext context)
        {
            Console.WriteLine(request.UserName + " has disconected!");
            responses.Remove(request.Id);
            return Task.FromResult(new Empty() { });
        }
    }
}
