﻿using Grpc.Core;
using Generated;
using System;

namespace Server
{
    class Program
    {
        public static void modifyClientCount()
        {
            System.IO.File.WriteAllText("..\\..\\..\\ClientCounter/ClientCounter.txt", string.Empty);
            System.IO.StreamWriter fileWriter = new System.IO.StreamWriter("..\\..\\..\\ClientCounter/ClientCounter.txt");
            fileWriter.WriteLine(0);
            fileWriter.Close();
        }
        static void Main(string[] args)
        {
            using (Server server = new Server(Configuration.HOST, Configuration.PORT))
            {
                server.CloseServerAction = () => Console.ReadKey();
                server.Start();
                modifyClientCount();
            }
        }
    }
}
